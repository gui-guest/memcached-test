Repository to test memcached Debian packaging only.
This launch a pipeline which:
- build package with last code from git
- test the package with debian/tests/
- execute these jobs for `unstable` and `stretch`
